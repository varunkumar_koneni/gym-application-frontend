import React, { useEffect, useState, ChangeEvent, FormEvent } from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import TraineeHeader from './TraineeHeader';
import Footer from './Footer';
import FormControlLabel from '@mui/material/FormControlLabel';
import RadioGroup from '@mui/material/RadioGroup';
import Radio from '@mui/material/Radio';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';

interface Trainer {
  username: string;
  firstName: string;
  lastName: string;
  specialization: string;
}

interface Trainee {
  firstName: string;
  lastName: string;
  username: string;
  dateOfBirth: string;
  address: string;
  active: boolean;
}

interface NotAssignedTrainer {
  username: string;
  firstName: string;
  lastName: string;
  specialization: string;
}

const theme = createTheme();

const UpdateTraineeProfile: React.FC = () => {
  const username = localStorage.getItem('username');

  const [traineeData, setTraineeData] = useState<Trainee | null>(null);
  const [notAssignedTrainers, setNotAssignedTrainers] = useState<NotAssignedTrainer[]>([]);
  const [selectedTrainers, setSelectedTrainers] = useState<string[]>([]);

  useEffect(() => {
    fetch(`http://localhost:8080/trainees/${username}`)
      .then(response => response.json())
      .then(data => setTraineeData(data))
      .catch(error => {
        window.location.href = '/notFound';
        console.error('Error fetching trainee data:', error);
      });

    fetch(`http://localhost:8080/trainees/not-assigned-trainers?username=${username}`)
      .then(response => response.json())
      .then(data => setNotAssignedTrainers(data))
      .catch(error => {
        console.error('Error fetching not assigned trainers:', error);
      });
  }, [username]);

  const handleCheckboxChange = (trainerUsername: string) => {
    const updatedSelection = [...selectedTrainers];
    const index = updatedSelection.indexOf(trainerUsername);

    if (index === -1) {
      updatedSelection.push(trainerUsername);
    } else {
      updatedSelection.splice(index, 1);
    }

    setSelectedTrainers(updatedSelection);
  };

  const handleUpdateButtonClick = () => {
    console.log('Updating trainers:', selectedTrainers);
    const data = {
      traineeUsername: username,
      trainersList: selectedTrainers,
    };
    console.log(data);

    // Example of making a PUT request
    fetch('http://localhost:8080/trainees/update-trainers', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => {
        if (response.ok) {
          console.log('Trainers updated successfully.');
        } else {
          console.error('Failed to update trainers.');
        }
      })
      .catch(error => {
        console.error('Error during update:', error);
      });
  };

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;

    setTraineeData((prevData) => ({
      ...prevData!,
      [name]: value,
    }));
  };

  const handleRadioChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    
    setTraineeData((prevData) => ({
      ...prevData!,
      active: value === 'true',
    }));
  };

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    
    const username = localStorage.getItem('username');
    
    const updatedTraineeData = {
      username: username,
      firstName: traineeData?.firstName,
      lastName: traineeData?.lastName,
      dateOfBirth: traineeData?.dateOfBirth,
      address: traineeData?.address,
      active: traineeData?.active,
    };

    try {
      const response = await fetch(`http://localhost:8080/trainees`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedTraineeData),
      });

      if (response.ok) {
        console.log('Trainer profile updated successfully');
      } else {
        console.error('Failed to update trainer profile');
      }
    } catch (error) {
      console.error('Error during update:', error);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <TraineeHeader />
      <Paper elevation={5} sx={{ padding: 3, height: '50%', width: '70%', margin: '0 auto', marginTop: '10px' }}>
      <Typography component="h1" variant="h5" align="center" sx={{ marginBottom: 5 }}>
                Update Trainee Profile
              </Typography>
      <Container sx={{ marginTop: 4 }}>
        <Grid container spacing={3} sx={{ mt: 3 }}>
          <Grid  item xs={10} md={4}>

              <form onSubmit={handleSubmit}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  name="firstName"
                  autoComplete="given-name"
                  value={traineeData?.firstName || ''}
                  onChange={handleInputChange}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="family-name"
                  value={traineeData?.lastName || ''}
                  onChange={handleInputChange}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  label="Date of Birth"
                  type="date"
                  name="dateOfBirth"
                  value={traineeData?.dateOfBirth || ''}
                  onChange={handleInputChange}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <TextField
                  margin="normal"
                  fullWidth
                  id="address"
                  label="Address"
                  name="address"
                  autoComplete="address"
                  value={traineeData?.address || ''}
                  onChange={handleInputChange}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />

                <RadioGroup
                  row
                  name="isActive"
                  value={traineeData?.active ? 'true' : 'false'}
                  onChange={handleRadioChange}
                  sx={{ mt: 2 }}
                >
                  <FormControlLabel value="true" control={<Radio />} label="Active" />
                  <FormControlLabel value="false" control={<Radio />} label="Inactive" />
                </RadioGroup>

                <Button type="submit" fullWidth variant="contained" sx={{ mt: 3 }}>
                  Update Profile
                </Button>
              </form>
          </Grid>
          <Grid item xs={12} md={6} marginLeft={20}>
              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell></TableCell>
                      <TableCell>First Name</TableCell>
                      <TableCell>Last Name</TableCell>
                      <TableCell>Specialization</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {notAssignedTrainers.map((trainer, index) => (
                      <TableRow key={index}>
                        <TableCell>
                          <Checkbox
                            checked={selectedTrainers.includes(trainer.username)}
                            onChange={() => handleCheckboxChange(trainer.username)}
                          />
                        </TableCell>
                        <TableCell>{trainer.firstName}</TableCell>
                        <TableCell>{trainer.lastName}</TableCell>
                        <TableCell>{trainer.specialization}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <Button onClick={handleUpdateButtonClick} sx={{ mt: 3 }}>Update Selected Trainers</Button>
          </Grid>
        </Grid>
      </Container>
      </Paper>
      <Footer />

    </ThemeProvider>
  );
};

export default UpdateTraineeProfile;
