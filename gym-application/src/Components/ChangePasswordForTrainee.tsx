import React, { useState } from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import Footer from './Footer';
import Header from './Header';
import TrainerHeader from './TrainerHeader';
import TraineeHeader from './TraineeHeader';

const theme = createTheme();

const ChangePasswordForTrainee = () => {

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  
    const formData = new FormData(event.currentTarget);
  
    const username = formData.get('username');
    const oldPassword = formData.get('oldPassword');
    const newPassword = formData.get('newPassword');
    const newPasswordConfirmation = formData.get('newPasswordConfirmation');

    if (newPasswordConfirmation !== newPassword) {
      alert('password does not match');
      return;
    }
  
    try {
      const response = await fetch(`http://localhost:8080/login/update?username=${username}&oldPassword=${oldPassword}&newPassword=${newPassword}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });
  
      if (response.ok) {

        alert('Password changed successfully');
      } else {
        console.error('Password change failed');
      }
    } catch (error) {
      console.error('Error changing password:', error);
    }
  };
  
  

  return (
    <ThemeProvider theme={theme}>
      <TraineeHeader/>
      <Container component="main" maxWidth="xs" sx={{ marginTop: 8, display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
        <Typography component="h1" variant="h5" align="center">
          Change Password
        </Typography>
        <form onSubmit={handleSubmit}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="oldPassword"
            label="Old Password"
            type="password"
            id="oldPassword"
            autoComplete="current-password"
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="newPassword"
            label="New Password"
            type="password"
            id="newPassword"
            autoComplete="new-password"
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="newPasswordConfirmation"
            label="New Password Confirmation"
            type="password"
            id="newPasswordConfirmation"
            autoComplete="new-password"
          />
          <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, width: '50%' }}>
            Change Password
          </Button>
        </form>
      </Container>
      <Footer/>
    </ThemeProvider>
  );
};

export default ChangePasswordForTrainee;
