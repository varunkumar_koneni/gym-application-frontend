// TrainerRegistration.jsx
import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import Header from './Header';
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';

interface TrainerCredentials {
  username: string;
  password: string;
}

const theme = createTheme();

const TrainerRegistration = () => {
  const navigate = useNavigate();
  const [registrationSuccess, setRegistrationSuccess] = useState(false);
  const [credentials, setCredentials] = useState<TrainerCredentials | undefined>();

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const formData = new FormData(event.currentTarget);
    const trainerData = {
      firstName: formData.get('firstName'),
      lastName: formData.get('lastName'),
      email: formData.get('email'),
      specialization: formData.get('specialization'),
    };

    try {
      const response = await fetch('http://localhost:8080/trainers/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(trainerData),
      });

      if (response.ok) {
        const credentialsResponse: TrainerCredentials = await response.json();
        setCredentials(credentialsResponse);
        
        setRegistrationSuccess(true);
      } else {
        console.error('Registration failed');
      }
    } catch (error) {
      console.error('Error during registration:', error);
    }
  };

  const handleButtonClick = () => {
    if (credentials) {
      localStorage.setItem('username', credentials.username);
      navigate('/TrainerProfile'); 
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Header />
      <Container component="main" maxWidth="xs" sx={{ marginTop: 8, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <Typography component="h1" variant="h5">
          {registrationSuccess ? 'Registration Successful' : 'Trainer Registration'}
        </Typography>

        {registrationSuccess ? (
          <>
            <TextField
              margin="normal"
              fullWidth
              id="username"
              label="Username"
              name="username"
              value={credentials?.username || ''}
              InputProps={{
                readOnly: true,
              }}
            />
            <TextField
              margin="normal"
              fullWidth
              id="password"
              label="Password"
              name="password"
              value={credentials?.password || ''}
              InputProps={{
                readOnly: true,
              }}
            />
            
            <Button onClick={handleButtonClick} fullWidth variant="contained" sx={{ mt: 3, width: '50%' }}>
              Next
            </Button>
          </>
        ) : (
          <form onSubmit={handleSubmit}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="firstName"
              label="First Name"
              name="firstName"
              autoComplete="given-name"
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="lastName"
              label="Last Name"
              name="lastName"
              autoComplete="family-name"
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email"
              name="email"
              type="email"
              autoComplete="email"
            />
            <FormControl fullWidth margin="normal" required>
              <InputLabel id="specialization-label">Specialization</InputLabel>
              <Select
                labelId="specialization-label"
                id="specialization"
                label="Specialization"
                name="specialization"
              >
                <MenuItem value="fitness">Fitness</MenuItem>
                <MenuItem value="yoga">Yoga</MenuItem>
                <MenuItem value="zumba">Zumba</MenuItem>
                <MenuItem value="stretching">Stretching</MenuItem>
                <MenuItem value="resistance">Resistance</MenuItem>
              </Select>
            </FormControl>
            <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, width: '50%' }}>
              Register
            </Button>
          </form>
        )}
      </Container>
      <Footer />
    </ThemeProvider>
  );
};

export default TrainerRegistration;
