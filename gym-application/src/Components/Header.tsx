// import * as React from 'react';
// import AppBar from '@mui/material/AppBar';
// import Box from '@mui/material/Box';
// import Toolbar from '@mui/material/Toolbar';
// import IconButton from '@mui/material/IconButton';
// import Typography from '@mui/material/Typography';
// import Menu from '@mui/material/Menu';
// import MenuIcon from '@mui/icons-material/Menu';
// import Container from '@mui/material/Container';
// import Button from '@mui/material/Button';
// import MenuItem from '@mui/material/MenuItem';
// import FitnessCenterIcon from '@mui/icons-material/FitnessCenter';

// const pages = [
//     {
//      name : 'Register As Trainee',
//      url : '/TraineeRegistration'
//     },
//     {
//      name : 'Register As Trainer',
//      url : '/TrainerRegistration'
//     },
//     {
//      name : 'Log in',
//      url : '/Login'
//     }
// ];

// const Header = () => {
//     const [anchorElNav, setAnchorElNav] = React.useState(null);

//     const handleOpenNavMenu = () => {
//         //setAnchorElNav(event.currentTarget);
//     };

//     const handleCloseNavMenu = () => {
//         setAnchorElNav(null);
//     };

//     return (
//         <AppBar position="static">
//             <Container maxWidth="xl">
//                 <Toolbar disableGutters>
//                     <Button href="/" sx={{
//                         color: 'white',
//                         display: { xs: 'none', md: 'flex' }
//                     }}
//                         startIcon={<FitnessCenterIcon sx={{ display: { xs: 'none', md: 'flex' } }} />}>
//                         <Typography
//                             variant="h6"
//                             noWrap
//                             component="a"
//                             href="/"
//                             sx={{
//                                 mr: 2,
//                                 display: { xs: 'none', md: 'flex' },
//                                 fontFamily: 'monospace',
//                                 fontWeight: 700,
//                                 color: 'inherit',
//                                 textDecoration: 'none',
//                             }}
//                         >
//                             Fitness Gym
//                         </Typography>
//                     </Button>

//                     <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
//                         <IconButton
//                             size="large"
//                             aria-label="account of current user"
//                             aria-controls="menu-appbar"
//                             aria-haspopup="true"
//                             onClick={handleOpenNavMenu}
//                             color="inherit"
//                         >
//                             <MenuIcon />
//                         </IconButton>
//                         <Menu
//                             id="menu-appbar"
//                             anchorEl={anchorElNav}
//                             anchorOrigin={{
//                                 vertical: 'bottom',
//                                 horizontal: 'left',
//                             }}
//                             keepMounted
//                             transformOrigin={{
//                                 vertical: 'top',
//                                 horizontal: 'left',
//                             }}
//                             open={Boolean(anchorElNav)}
//                             onClose={handleCloseNavMenu}
//                             sx={{
//                                 display: { xs: 'block', md: 'none' },
//                             }}
//                         >
//                             {pages.map((page) => (
//                                 <MenuItem key={page.name} onClick={handleCloseNavMenu}>
//                                     <Typography textAlign="center">{page.name}</Typography>
//                                 </MenuItem>
//                             ))}
//                         </Menu>
//                     </Box>
//                     <FitnessCenterIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} />
//                     <Typography
//                         variant="h5"
//                         noWrap
//                         component="a"
//                         href="#app-bar-with-responsive-menu"
//                         sx={{
//                             ml: 1,
//                             mr: 2,
//                             display: { xs: 'flex', md: 'none' },
//                             flexGrow: 1,
//                             fontFamily: 'monospace',
//                             fontWeight: 700,
//                             letterSpacing: '.3rem',
//                             color: 'inherit',
//                             textDecoration: 'none',
//                         }}
//                     >
//                         Fitness Gym
//                     </Typography>
                    
//                     <Box sx={{ flexGrow: 1, display: { xs: 'none', justifyContent: 'flex-end', md: 'flex' } }}>
//                         {pages.map((page) => (
//                             <Button
//                                 variant='contained'
//                                 key={page.name}
//                                 onClick={handleCloseNavMenu}
//                                 sx={{ my: 2, mr: 1, color: 'white', display: 'block' }}
//                                 href={page.url}
//                             >
//                                 {page.name}
//                             </Button>
//                         ))}
//                     </Box>


//                 </Toolbar>
//             </Container>
//         </AppBar>
//     );
// }
// export default Header;

















import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import HomeIcon from '@mui/icons-material/Home';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import LockIcon from '@mui/icons-material/Lock';
import AdbIcon from '@mui/icons-material/Adb';


const pages = [
  { label: 'Home', icon: <HomeIcon />, url: '#' },
  { label: 'Trainee Registration', icon: <PersonAddIcon />, url: 'TraineeRegistration' },
  { label: 'Trainer Registration', icon: <PersonAddIcon />, url: 'TrainerRegistration' },
  { label: 'Login', icon: <LockIcon />, url: '/Login' },
];


const Header = () => {
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <AdbIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} />
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="#app-bar-with-responsive-menu"
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            GYM-APP
          </Typography>

          <Box sx={{ flexGrow: 1 }} />


          <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
            {pages.map((page) => (
              <Button
                key={page.label}
                component="a"
                href={page.url}
                startIcon={page.icon}
                onClick={handleCloseNavMenu}
                sx={{ mx: 1, color: 'white', textDecoration: 'none' }}
              >
                {page.label}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Header;