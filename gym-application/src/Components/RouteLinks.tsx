// RouteLinks.jsx
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import TrainerRegistration from './TrainerRegistration';
import TraineeRegistration from './TraineeRegistration';
import Login from './Login';
import TraineeProfile from './TraineeProfile';
import TrainingsLogForTrainee from './TrainingsLogForTrainee';
import TrainingsLogForTrainer from './TrainingsLogForTrainer';
import AddTrainingForTrainee from './AddTrainingForTrainee';
import UpdateTrainerProfile from './UpdateTrainerProfile';
import UpdateTraineeProfile from './UpdateTraineeProfile';
import TrainerProfile from './TrainerProfile';
import ChangePasswordForTrainer from './ChangePasswordForTrainer';
import ChangePasswordForTrainee from './ChangePasswordForTrainee';
import LogoutButton from './LogoutButton';
import HomePage from './HomePage';

const RouteLinks = () => {
  return (
    <Router>
      <Routes>
        <Route path="/TrainerRegistration" element={<TrainerRegistration />} />
        <Route path="/TraineeRegistration" element={<TraineeRegistration />} />
        <Route path="/Login" element={<Login />} />
        <Route path="/LogoutButton" element={<LogoutButton />} />
        <Route path="/HomePage" element={<HomePage />} />

        <Route path="/TraineeProfile" element={<TraineeProfile />} />
        <Route path="/TrainerProfile" element={<TrainerProfile />} />
        <Route path="/ChangePasswordForTrainee" element={<ChangePasswordForTrainee />} />
        <Route path="/ChangePasswordForTrainer" element={<ChangePasswordForTrainer />} />
        <Route path="/TrainingsLogForTrainee" element={<TrainingsLogForTrainee />} />
        <Route path="/TrainingsLogForTrainer" element={<TrainingsLogForTrainer />} />
        <Route path="/AddTrainingForTrainee" element={<AddTrainingForTrainee />} />
        <Route path="/UpdateTraineeProfile" element={<UpdateTraineeProfile />} />
        <Route path="/UpdateTrainerProfile" element={<UpdateTrainerProfile />} />
       </Routes>
    </Router>
  );
};

export default RouteLinks;
