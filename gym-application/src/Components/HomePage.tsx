import React from 'react';
import Header from './Header';
import TraineeHeader from './TraineeHeader';
import TrainerHeader from './TrainerHeader';
import Footer from './Footer';
import { Grid, Typography } from '@mui/material';

import './HomePage.css';


const HomePage: React.FC = () => {
  const userType = localStorage.getItem('userType');
  console.log(userType);

  const renderCommonContent = (): JSX.Element => {
    return (
      <Grid container className="home-content-container">
      <Grid item xs={6}>
        <div className="home-content">
          <Typography variant="h4" >
            <p className="home-content-heading">Welcome to GymApp</p>
          </Typography>
          <Typography variant="body1">
            Whether you're a fitness enthusiast or just starting your fitness journey,
            GymApp has everything you need to achieve your fitness goals.
            Explore our training programs, connect with certified trainers, and track
            your progress. It's time to transform your life and get fit!
          </Typography>
          <Typography variant="body1" style={{fontWeight:'bold'}}>Get started by signing up or logging in to your account.</Typography>
        </div>
      </Grid>

      <Grid item xs={6}>
        <img src='gym-home.PNG' alt="Gym" className="home-image" />
      </Grid>
      <Footer/>


    </Grid>


    );
  };

  let headerComponent: JSX.Element;
  switch (userType) {
    case 'trainee':
      headerComponent = <TraineeHeader />;
      break;
    case 'trainer':
      headerComponent = <TrainerHeader />;
      break;
    default:
      headerComponent = <Header />;
      break;
  }

  return (
    <div>
      {headerComponent}

      {renderCommonContent()}

    </div>
  );
};

export default HomePage;
