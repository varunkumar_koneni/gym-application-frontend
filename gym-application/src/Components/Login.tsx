import * as React from 'react';
import {
  Box,
  Button,
  Container,
  IconButton,
  Link,
  Paper,
  TextField,
  Typography,
} from '@mui/material';
import { Refresh as RefreshIcon } from '@mui/icons-material';
import Header from './Header';
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';
import FormControlLabel from '@mui/material/FormControlLabel';
import RadioGroup from '@mui/material/RadioGroup';
import Radio from '@mui/material/Radio';
import { ThemeProvider, createTheme } from '@mui/material/styles';

const theme = createTheme();

const Login = () => {
  const [captcha, setCaptcha] = React.useState(generateCaptcha());
  const [enteredCaptcha, setEnteredCaptcha] = React.useState('');
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [userType, setUserType] = React.useState('trainer'); // Default to 'trainer'
  const navigate = useNavigate(); // useNavigate from react-router-dom

  function generateCaptcha() {
    return Math.floor(100000 + Math.random() * 900000).toString();
  }

  const handleRefreshCaptcha = () => {
    setCaptcha(generateCaptcha());
    setEnteredCaptcha('');
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (enteredCaptcha !== captcha) {
      alert('CAPTCHA verification failed. Please try again.');
      return;
    }

    const loginData = {
      username: username,
      password: password,
    };

    // Determine the endpoint based on the selected user type

    try {
      const response = await fetch(`http://localhost:8080/login?username=${loginData.username}&password=${loginData.password}`, {        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        console.log('Login successful');
        localStorage.setItem('username', loginData.username);


        navigate(userType === 'trainer' ? '/TrainerProfile' : '/TraineeProfile');
      } else {
        alert("login failed, wrong username or password or account does not exist");
      }
    } catch (error) {
      console.error('Error during login:', error);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Header />
      <Container component="main" maxWidth="xs" sx={{ marginTop: 5, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Paper elevation={3} sx={{ padding: 3, width: '95%', flexDirection: 'column', alignItems: 'center' }}>

        <Typography variant="h5" component="h1">
          Login
        </Typography>
        <Typography variant="h6" component="h1">
          Welcome Back
        </Typography>
        <form onSubmit={handleSubmit}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <RadioGroup
            row
            name="user"
            sx={{ mt: 2, display: 'flex', alignItems: 'center', marginLeft:10 }}
            value={userType}
            onChange={(e) => setUserType(e.target.value)}
          >
            <FormControlLabel value="trainer" control={<Radio />} label="Trainer" />
            <FormControlLabel value="trainee" control={<Radio />} label="Trainee" />
          </RadioGroup>


          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', mt: 0.5 }}>
            <TextField margin="normal" required disabled label="CAPTCHA" value={captcha} />
            <IconButton color="primary" onClick={handleRefreshCaptcha} sx={{ ml: 1 }}>
              <RefreshIcon />
            </IconButton>
            <TextField
              margin="normal"
              required
              fullWidth
              label="Enter CAPTCHA"
              value={enteredCaptcha}
              onChange={(e) => setEnteredCaptcha(e.target.value)}
            />
          </Box>

          <Typography variant="body2" component="div" sx={{ mt: 3 }}>
            <Link href="#" color="textSecondary">
              Forgot password?
            </Link>
          </Typography>
          <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, width: '50%' }}>
            Log In
          </Button>
        </form>
        </Paper>

      </Container>
      <Footer />
    </ThemeProvider>
  );
};

export default Login;
