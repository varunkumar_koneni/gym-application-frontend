import React, { useEffect, useState } from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import './TrainingTable.css';
import InputLabel from '@mui/material/InputLabel';
import TraineeHeader from './TraineeHeader';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import Footer from './Footer';

const theme = createTheme();




interface TraineeTrainingsResponseDTO {
  trainingName:string,
  trainingDate:Date,
  trainingType:string,
  duration: Number,
  trainerName :string
}

const TrainingsLog = () => {

  const username = localStorage.getItem('username');

    const [trainerName, setTrainerName] = useState('');
  const [specialization, setSpecialization] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [trainingsList, setTrainingsList] = useState<TraineeTrainingsResponseDTO[]>([]);

  const handleSearch = () => {
    const params = new URLSearchParams();
    if (username) params.append('username', username); 
    if (trainerName) params.append('trainerName', trainerName);
    if (specialization) params.append('trainingType', specialization);
    if (startDate) params.append('periodFrom', startDate);
    if (endDate) params.append('periodTo', endDate);

    fetch(`http://localhost:8080/trainees/trainingsList?${params.toString()}`)
      .then((response) => response.json())
      .then((data) => setTrainingsList(data))
      .catch((error) => console.error('Error fetching trainee trainings:', error));
  };

  useEffect(() => {
    handleSearch();
  }, []);
  console.log(trainingsList);



  return (
    <ThemeProvider theme={theme}>

    <TraineeHeader/>
    <Container sx={{ marginTop: 5}}>
      <Typography variant="h4" align="center" gutterBottom>
        Trainings Log for Trainee
      </Typography>
      <Box mb={2}>
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={12} sm={2.5}>
            <TextField
              label="Search by Trainer Name"
              value={trainerName}
              onChange={(e) => setTrainerName(e.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={2.5}>
            <FormControl fullWidth margin="normal">
            <InputLabel id="specialization-label">Specialization</InputLabel>
              <Select
                value={specialization}
                onChange={(e) => setSpecialization(e.target.value)}
                labelId="specialization-label"
              id="specialization"
              label="Specialization"
              name="specialization"
                
              >
                <MenuItem value="fitness">Fitness</MenuItem>
              <MenuItem value="yoga">Yoga</MenuItem>
              <MenuItem value="zumba">Zumba</MenuItem>
              <MenuItem value="stretching">Stretching</MenuItem>
              <MenuItem value="resistance">Resistance</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={2.5}>
            <TextField
              label="Start Date"
              type="date"
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
              fullWidth
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
          <Grid item xs={12} sm={2.5}>
            <TextField
              label="End Date"
              type="date"
              value={endDate}
              onChange={(e) => setEndDate(e.target.value)}
              fullWidth
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
          <Grid item xs={12} sm={2}>
            <Button variant="contained" onClick={handleSearch} fullWidth>
              Search
            </Button>
          </Grid>
        </Grid>
      </Box>
  <div>
    <table className="Table">
      <thead className="TableHead">
        <tr>
          <th className="TableCell">Trainer Name</th>
          <th className="TableCell">Training Name</th>
          <th className="TableCell">Training Date</th>
          <th className="TableCell">Training Type</th>
          <th className="TableCell">Duration</th>
        </tr>
      </thead>
      <tbody>
        {trainingsList.map((training, index) => (
          <tr className="TableRow" key={index}>
            <td className="TableCell">{training.trainerName}</td>
            <td className="TableCell">{training.trainingName}</td>
            <td className="TableCell">{training.trainingDate.toString()}</td>
            <td className="TableCell">{training.trainingType}</td>
            <td className="TableCell">{training.duration.toString()} mins</td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>    
    </Container>
    <Footer/>
    </ThemeProvider>

  );
};

export default TrainingsLog;
