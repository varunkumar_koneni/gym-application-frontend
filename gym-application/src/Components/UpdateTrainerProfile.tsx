import React, { useEffect, useState } from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import FormControlLabel from '@mui/material/FormControlLabel';
import RadioGroup from '@mui/material/RadioGroup';
import Radio from '@mui/material/Radio';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import TrainerHeader from './TrainerHeader';
import Footer from './Footer';

interface Trainer {
  firstName: string;
  lastName: string;
  specialization: string;
  active: boolean;
  trainees: Trainee[];
}

interface Trainee {
  firstName: string;
  lastName: string;
}

const theme = createTheme();

const UpdateTrainerProfile = () => {
  const [trainerData, setTrainerData] = useState<Trainer | null>(null);
  const username = localStorage.getItem('username');

  useEffect(() => {
    fetch(`http://localhost:8080/trainers/${username}`)
      .then(response => response.json())
      .then(data => setTrainerData(data))
      .catch(error => {
        console.error('Error fetching trainer data:', error);
      });
  }, [username]);

  const handleInputChange = (fieldName: string, value: string) => {
    setTrainerData((prevData) => ({
      ...prevData!,
      [fieldName]: value,
    }));
  };
  const handleRadioChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    
    setTrainerData((prevData) => ({
      ...prevData!,
      active: value === 'true',
    }));
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    // Prepare the data for the update
    const updatedData = {
      username: username,
      firstName: trainerData?.firstName,
      lastName: trainerData?.lastName,
      active: trainerData?.active,
    };

    try {
      const response = await fetch(`http://localhost:8080/trainers`, {
        method: 'PUT', // or 'PATCH' depending on your API
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedData),
      });

      if (response.ok) {
        console.log('Trainer profile updated successfully');
        // You might want to handle success, maybe redirect or show a success message
      } else {
        console.error('Failed to update trainer profile');
        // You might want to handle the error, maybe show an error message
      }
    } catch (error) {
      console.error('Error during update:', error);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <TrainerHeader/>
      <Container component="main" maxWidth="xs" sx={{ marginTop: 8, display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
        <Typography component="h1" variant="h5" align="center" sx={{ mb: 3 }}>
          Update Trainer Profile
        </Typography>
        <form onSubmit={handleSubmit}>
          <TextField
            fullWidth
            label="First Name"
            name="firstName"
            autoComplete="given-name"
            value={trainerData?.firstName || ''}
            onChange={(e) => handleInputChange('firstName', e.target.value)}
            sx={{ mb: 2 }}
          />
          <TextField
            fullWidth
            label="Last Name"
            name="lastName"
            autoComplete="family-name"
            value={trainerData?.lastName || ''}
            onChange={(e) => handleInputChange('lastName', e.target.value)}
            sx={{ mb: 2 }}
          />
          <RadioGroup
            row
            name="isActive"
            value={trainerData?.active ? 'true' : 'false'}
            onChange={handleRadioChange}
            sx={{ mt: 2, display: 'flex', alignItems: 'center', marginLeft:12 }}
            >
            <FormControlLabel value="true" control={<Radio />} label="Active" />
            <FormControlLabel value="false" control={<Radio />} label="Inactive" />
          </RadioGroup>


      
          <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, width: '50%' }}>
            Update
          </Button>
        </form>
      </Container>
      <Footer/>
    </ThemeProvider>
  );
};

export default UpdateTrainerProfile;
